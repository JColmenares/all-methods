package com.examples.archive;

import java.io.File;
import java.io.FileReader;
import java.util.List;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.properties.propertiesloader.EnviromentUtil;

public class ReadArchiveCVS {
	
	public static final String NAMEFILEPROPERTIE = "codes-functional/Methods/src/main/resources";

	//PAGINA DE REFERENCIA: https://www.campusmvp.es/recursos/post/como-leer-y-escribir-archivos-csv-con-java.aspx
	
	
	public static void main(String[] args) throws Exception {
		
		//readArchiveCVSIndividual("C:\\Users\\Developer\\code\\codes-functional\\Methods\\src\\main\\resources\\prueba-java.csv");

		//readArchiveCVSIndividualForFields("C:\\Users\\Developer\\code\\codes-functional\\Methods\\src\\main\\resources\\prueba-java.csv");
		
		//readArchiveCVSAllForFields("C:\\Users\\Developer\\code\\codes-functional\\Methods\\src\\main\\resources\\prueba-java.csv");
		
		readArchiveCVSForPropertiesLoader("/prueba-java.csv");
	}
	 
	public static void readArchiveCVSIndividual(String routeArchive) throws Exception {
		
		System.out.println("Inicio de Metodo de lectura de archivo CVS");
		
		CSVReader csvReader = null;
		
		try {
			
			//VARIABLE PARA VALIDAR SI EL ARCHIVO EXISTE EN EL DIRECTORIO
			File fileVerification = new File(routeArchive);
						
			//VALIDACION DE EXISTENCIA DE ARCHIVO EN DIRECTORIO
			if (fileVerification.exists()) {
			
			
				//CSVParser conPuntoYComa = new CSVParserBuilder().withSeparator(';').build();
				csvReader = new CSVReader(new FileReader(routeArchive));//.withCSVParser(conPuntoYComa).build();
	
				String[] fila = null;
				while((fila = csvReader.readNext()) != null) {
				    System.out.println(fila[0]);
				    
				}
				
			} else {
				System.err.println("ERROR: EL ARCHIVO A LEER NO EXISTE EN EL DIRECTORIO.");
				throw new Exception("ERROR: EL ARCHIVO A LEER NO EXISTE EN EL DIRECTORIO.");
			}
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally {
			csvReader.close();
		}
		
		System.out.println("Fin de Metodo de lectura de archivo CVS");
		
	}
	
	
	public static void readArchiveCVSIndividualForFields(String routeArchive) throws Exception {
		
		System.out.println("Inicio de Metodo de lectura de archivo CVS delimitado por punto y comas");
		
		CSVReader csvReader = null;
		
		try {
			
			//VARIABLE PARA VALIDAR SI EL ARCHIVO EXISTE EN EL DIRECTORIO
			File fileVerification = new File(routeArchive);
						
			//VALIDACION DE EXISTENCIA DE ARCHIVO EN DIRECTORIO
			if (fileVerification.exists()) {
			
				CSVParser conPuntoYComa = new CSVParserBuilder().withSeparator(';').build();
				csvReader = new CSVReaderBuilder(new FileReader(routeArchive)).withCSVParser(conPuntoYComa).build();
	
				String[] fila = null;
				while((fila = csvReader.readNext()) != null) {
				    System.out.println(fila[0] + " " + fila[1] + " " + fila[2] + " " + fila[3] + " " + fila[4] + " " + fila[5]);
				    
				}
			} else {
				System.err.println("ERROR: EL ARCHIVO A LEER NO EXISTE EN EL DIRECTORIO.");
				throw new Exception("ERROR: EL ARCHIVO A LEER NO EXISTE EN EL DIRECTORIO.");
			}	
				
		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally {
			csvReader.close();
		}
		
		System.out.println("Fin de Metodo de lectura de archivo CVS");
		
	}
	
	
	public static void readArchiveCVSAllForFields(String routeArchive) throws Exception {
		
		System.out.println("Inicio de Metodo de lectura de archivo CVS delimitado por punto y comas");
		
		CSVReader csvReader = null;
		
		try {
			
			//VARIABLE PARA VALIDAR SI EL ARCHIVO EXISTE EN EL DIRECTORIO
			File fileVerification = new File(routeArchive);
						
			//VALIDACION DE EXISTENCIA DE ARCHIVO EN DIRECTORIO
			if (fileVerification.exists()) {
			
				CSVParser conPuntoYComa = new CSVParserBuilder().withSeparator(';').build();
				csvReader = new CSVReaderBuilder(new FileReader(routeArchive)).withCSVParser(conPuntoYComa).build();
	
				List<String[]> datos = csvReader.readAll();
				
				for (String[] row : datos) {
					
					System.out.println("Fila: " + row);
					
					for (String col : row) {
						
						System.out.println("Columnas: " + col);
						
					}
					
				}
			} else {
				System.err.println("ERROR: EL ARCHIVO A LEER NO EXISTE EN EL DIRECTORIO.");
				throw new Exception("ERROR: EL ARCHIVO A LEER NO EXISTE EN EL DIRECTORIO."); 
			}
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally {
			csvReader.close();
		}
		
		System.out.println("Fin de Metodo de lectura de archivo CVS");
		
	}
	
	
	public static void readArchiveCVSForPropertiesLoader(String nameArchive) throws Exception {
		
		System.out.println("Inicio de Metodo de lectura de archivo CVS delimitado por punto y comas, Uando PropertiesLoader");
		
		CSVReader csvReader = null;
		String routeArchive = null;
		
		try {
			 
			routeArchive = EnviromentUtil.getAbsoluteResourceName(NAMEFILEPROPERTIE + nameArchive);
			System.out.println("Ruta de busqueda de archivo CSV: " + routeArchive);
			
			//VARIABLE PARA VALIDAR SI EL ARCHIVO EXISTE EN EL DIRECTORIO
			File fileVerification = new File(routeArchive);
						
			//VALIDACION DE EXISTENCIA DE ARCHIVO EN DIRECTORIO
			if (fileVerification.exists()) {
				
				CSVParser conPuntoYComa = new CSVParserBuilder().withSeparator(';').build();
				csvReader = new CSVReaderBuilder(new FileReader(routeArchive)).withCSVParser(conPuntoYComa).build();

				List<String[]> datos = csvReader.readAll();
				
				
				//METODO DE LECTURA 1			
				System.out.println("Inicio - Metodo de Lectura 1");
				for (String[] row : datos) {
					
					System.out.println("Fila: " + row);
					
					for (String col : row) {
						
						System.out.println("Columnas: " + col);
						
					}
					
				}
				
				csvReader.close();
				System.out.println("Fin - Metodo de Lectura 1");
				
				//METODO DE LECTURA 2
				csvReader = new CSVReaderBuilder(new FileReader(routeArchive)).withCSVParser(conPuntoYComa).build();
				
				System.out.println("Inicio - Metodo de Lectura 2");
				String[] fila = null;
				while((fila = csvReader.readNext()) != null) {
				    System.out.println(fila[0] + " " + fila[1] + " " + fila[2] + " " + fila[3] + " " + fila[4] + " " + fila[5]);
				    
				}
				System.out.println("Fin - Metodo de Lectura 2");
				
			} else {
				System.err.println("ERROR: EL ARCHIVO A LEER NO EXISTE EN EL DIRECTORIO.");
				throw new Exception("ERROR: EL ARCHIVO A LEER NO EXISTE EN EL DIRECTORIO."); 
			}
			
			
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally {
			csvReader.close();
		}
		
		System.out.println("Fin de Metodo de lectura de archivo CVS");
		
	}

}
