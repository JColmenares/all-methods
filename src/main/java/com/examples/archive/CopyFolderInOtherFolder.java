package com.examples.archive;

import java.io.File;

import org.apache.commons.io.FileUtils;

public class CopyFolderInOtherFolder {

	public static void main(String[] args) throws Exception {
		
		String filePathToCopy = "C:\\Users\\Developer\\code\\codes-functional\\Methods\\src\\main\\resources\\";
		String destinationPath = "C:\\Users\\Developer\\code\\codes-functional\\Methods\\src\\main\\resources\\pruebaCopy\\";
		
		
		copyFolderInOtherFolder(filePathToCopy, destinationPath);
		

	}
	
	
	public static void copyFolderInOtherFolder(String folderPathToCopy, String destinationPath) throws Exception {
		
		System.out.println("Inicio de Metodo para copiar Todos los archivos de una carpeta a otra");
		
		StringBuilder sms = new StringBuilder();
		sms.append("Paramentros de Entrada: \n");
		sms.append("Ruta de Carpeta a Copiar: ");
		sms.append(folderPathToCopy);
		sms.append(" \nRuta de Carpeta de Destino: ");
		sms.append(destinationPath);
		System.out.println(sms);
		
		File srcDir = new File(folderPathToCopy);
		File destDir = new File(destinationPath);
		
		try {
			FileUtils.copyDirectory(srcDir, destDir);
			System.out.println("Los Archivos de la carpeta han Sido Copiado Exitosamente en el Directorio Espeficicado.");
		} catch (Exception e) {
			System.err.println("ERROR: SE GENERO UN PROBLEMA AL COPIAR TODOS LOS ARCHIVOS DE LA CARPETA EN EL NUEVO DIRECTORIO.");
			System.err.println("ERROR GENERADO: " + e.getMessage());
			e.printStackTrace(); 
		}
		
		System.out.println("Fin de Metodo para copiar Todos los archivos de una carpeta a otra");
		
	}

}
