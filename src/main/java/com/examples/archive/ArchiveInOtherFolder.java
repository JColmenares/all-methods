package com.examples.archive;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


public class ArchiveInOtherFolder {

	//PAGINAS DE REFERENCIA: https://es.stackoverflow.com/questions/52012/copiar-archivos-de-una-carpeta-a-otra
		//					 https://es.stackoverflow.com/questions/15752/c%C3%B3mo-saber-si-existe-un-fichero-en-java			 
	
	
	public static void main(String[] args) throws Exception {

//		String filePathToCopy = "C:\\Users\\Developer\\code\\codes-functional\\Methods\\src\\main\\resources\\prueba-java.csv";
//		String destinationPath = "C:\\Users\\Developer\\code\\codes-functional\\Methods\\src\\main\\resources\\pruebaCopy\\prueba-java.csv";
//		
//		copyArchiveInOtherFolder(filePathToCopy, destinationPath);
		
		String archivePathToMove = "C:\\Users\\Developer\\code\\codes-functional\\Methods\\src\\main\\resources\\prueba-java.csv";
		String destinationPath = "C:\\Users\\Developer\\code\\codes-functional\\Methods\\src\\main\\resources\\pruebaCopy\\prueba-java.csv";
		
		
		moveArchiveInOtherFolder(archivePathToMove, destinationPath);
	
	}
	
	public static void copyArchiveInOtherFolder(String ArchivePathToCopy, String destinationPath) throws Exception {
		
		System.out.println("Inicio de Metodo Para Copiar Un Archivo de Una Carpeta a Atra");
		
		StringBuilder sms = new StringBuilder();
		sms.append("Paramentros de Entrada: \n");
		sms.append("Ruta y Archivo a Copiar: ");
		sms.append(ArchivePathToCopy);
		sms.append(" \nRuta de Carpeta Destino: ");
		sms.append(destinationPath);
		System.out.println(sms);
		
		//VARIABLES PARA LEER ARCHIVOS
		Path srcDir = Paths.get(ArchivePathToCopy);
		Path destDir = Paths.get(destinationPath);
		
		//VARIDACION DE ARCHIVO EN DIRECTORIO EMISOR
		File fileVerificationEmis = new File(ArchivePathToCopy);
		
		//Variable para validar Existencia de archivo en directorio destino
		File fileVerification = new File(destinationPath);
		
		try {
			
			//VALIDACION DE EXISTENCIA DE ARCHIVO EN DIRECTORIO EMISOR
			if (fileVerificationEmis.exists()) {
				
				//VALIDACIÓN DE EXISTENCIA DE ARCHIVO EN DIRECTORIO DESTINO
				if (!fileVerification.exists()) {
					//COPIA ARCHIVO SI NO EXISTE EN EL DIRECTORIO DESTINO
					System.out.println("El Archivo No existe en el Directorio Destino.");
					System.out.println("Se Procedera a Copiar...");
					Files.copy(srcDir, destDir);
					System.out.println("El Archivo a Sido Copiado Exitosamente.");
				} else {
					//SOBREESCRIBE EL ARCHIVO SI EXITE EN EL DIRECTORIO DESTINO
					System.out.println("El Archivo Existe en el Directorio Destino.");
					System.out.println("Se Procedera a SobreEscribir");
					Files.copy(srcDir, destDir,StandardCopyOption.REPLACE_EXISTING);
					System.out.println("El Archivo a Sido SobreEscrito Exitosamente.");
				}
				
			}  else {
				System.err.println("ERROR: EL ARCHIVO A MOVER NO EXISTE EN EL DIRECTORIO EMISOR.");
				throw new Exception("ERROR: EL ARCHIVO A MOVER NO EXISTE EN EL DIRECTORIO EMISOR."); 
			}
			
		} catch (Exception e) {
			System.err.println("ERROR: SE GENERO UN PROBLEMA AL COPIAR EL ARCHIVO EN EL NUEVO DIRECTORIO.");
			System.err.println("ERROR GENERADO: " + e.getMessage());
			e.printStackTrace(); 
		}
		
		System.out.println("Fin de Metodo para Copiar Un Archivo de una Carpeta a Otra");
		
	}
	
	
	public static void moveArchiveInOtherFolder(String archivePathToMove, String destinationPath) throws Exception {
		
		System.out.println("Inicio de Metodo Para Mover Un Archivo de Una Carpeta a Otra");
		
		StringBuilder sms = new StringBuilder();
		sms.append("Paramentros de Entrada: \n");
		sms.append("Ruta y Archivo a Mover: ");
		sms.append(archivePathToMove);
		sms.append(" \nRuta de Carpeta Destino: ");
		sms.append(destinationPath);
		System.out.println(sms);
		
		//VARIABLES PARA LEER ARCHIVOS
		Path srcDir = Paths.get(archivePathToMove);
		Path destDir = Paths.get(destinationPath);
		
		//VARIDACION DE ARCHIVO EN DIRECTORIO EMISOR
		File fileVerificationEmis = new File(archivePathToMove);
		
		//Variable para validar Existencia de archivo en directorio destino
		File fileVerificationDest = new File(destinationPath);
		
		try {

			//VALIDACION DE EXISTENCIA DE ARCHIVO EN DIRECTORIO EMISOR
			if (fileVerificationEmis.exists()) {
				
				//VALIDACIÓN DE EXISTENCIA DE ARCHIVO EN DIRECTORIO DESTINO
				if (!fileVerificationDest.exists()) {
					//MUEVE ARCHIVO SI NO EXISTE EN EL DIRECTORIO DESTINO
					System.out.println("El Archivo No existe en el Directorio Destino.");
					System.out.println("Se Procedera a Mover...");
					Files.move(srcDir, destDir);
					System.out.println("El Archivo a Sido Trasladado Exitosamente.");
				} else {
					//SOBREESCRIBE EL ARCHIVO SI EXITE EN EL DIRECTORIO DESTINO
					System.out.println("El Archivo Existe en el Directorio Destino.");
					System.out.println("Se Procedera a SobreEscribir");
					Files.move(srcDir, destDir,StandardCopyOption.REPLACE_EXISTING);
					System.out.println("El Archivo a Sido SobreEscrito Exitosamente.");
				}
				
			} else {
				System.err.println("ERROR: EL ARCHIVO A MOVER NO EXISTE EN EL DIRECTORIO EMISOR.");
				throw new Exception("ERROR: EL ARCHIVO A MOVER NO EXISTE EN EL DIRECTORIO EMISOR.");
			}
			
			
			
		} catch (Exception e) {
			System.err.println("ERROR: SE GENERO UN PROBLEMA AL MOVER EL ARCHIVO EN EL NUEVO DIRECTORIO.");
			System.err.println("ERROR GENERADO: " + e.getMessage());
			e.printStackTrace(); 
		}
		
		System.out.println("Fin de Metodo para Mover Un Archivo de una Carpeta a Otra");
		
	}

}
