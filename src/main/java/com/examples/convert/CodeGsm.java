package com.examples.convert;

public class CodeGsm {

	
	public static String convertCodeGsm58in0412(String gsm) {
		
		String newGsm = null;
		
		if (gsm.substring(0,2).equals("58")) {
			
			System.out.println("Gsm a cambiar el codigo '58': " + gsm);
			newGsm = "0" + gsm.substring(2).toString();	
			System.out.println("Gsm con codidigo '0412' : " + newGsm);
		}
		
		return newGsm;
	}
	
	public static String convertCodeGsm0412in58(String gsm) {
		
		String newGsm = null;
		
		if (gsm.substring(0,1).equals("0")) {
			
			System.out.println("Gsm a cambiar el codigo '0412': " + gsm);
			newGsm = "58" + gsm.substring(1).toString();	
			System.out.println("Gsm con codidigo '58' : " + newGsm);
		}
		
		return newGsm;
	}
	
}
